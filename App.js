/** Imports: React */
import React, {useEffect} from 'react';
/** Imports: dependencie Redux */
import { Provider } from 'react-redux';
/** Imports: dependencie Redux persist*/
import { PersistGate } from 'redux-persist/integration/react';
/** Imports: Redux store */
import {store, persistedStore} from './src/store';
/** Imports: App's entrypoint */
import App from './src';
/** Imports: hide splash screen */
import SplashScreen from 'react-native-splash-screen';


export default () => {
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistedStore}>
        <App />
      </PersistGate>
    </Provider>
  )
}
