# Light Control App
App designed to control a [Wifi Lamp](https://gitlab.com/luca-guazzaroni/light-control/esp8266-firmware). Right now, **only works for Android.**

<img src="docs/images/scan-networks-screen.jpg" alt="" title="" height="400" />
<img src="docs/images/control-device-screen.jpg" alt="" title="" height="400" />
<img src="docs/images/settings-screen.jpg" alt="" title="" height="400" />


## Index
- [⚙️ Install](#install)
- [🏭 Debug and Release](#debug-and-release)
- [🗺️ Project Structure](#project-structure)
- [📦 Dependencies](#dependencies)

##  Install
Depending on your operating system, the instalation instructions will change.
Look at the [official documentation](https://reactnative.dev/docs/environment-setup) to install the enviroment.
**By now, the app only works for Android.**

After your enviroment is correctly installed, you should:

1. Create local.properties file in /android/ directory with this content:
```
sdk.dir=<path_to_android_sdk>
```

2. Search for phones or emulators
```
adb devices
```
for example, this command should return
```
* daemon not running; starting now at tcp:5037
* daemon started successfully
List of devices attached
13895be7        device
```

3. Install javascript packages
```
cd <path_to_this_repo>
npm install
```

4. Link packages
```
react-native link
```

## Debug and Release
For debug, run the following commands:
1. Execute the metro server
```
npx react-native start
```
2. Execute the Android build
```
npx react-native run-android
```

For release, run the following command
```
react-native run-android --variant release
```

## Project Structure
- [`android`](/android) Android files and configurations
- [`ios`](/ios) Ios files and configurations
- [`docs`](/docs) Documentation files and assets
- [`src`](/src) React Native source Code
    - [`components`](/src/components) Specific React components for the project
    - [`constants`](/src/constants) Constants like colors, icon's names and some styles
    - [`containers/screens`](/src/containers/screens) What is shown in the screens
    - [`navigation`](/src/navigation) React Navigation configuration
    - [`reducers`](/src/reducers) Redux state files
    - [`translations`](/src/translations) json files for multi-language support
    - [`utils`](/src/utils) Functions like api calls and permission request
    - [`store.js`](/src/store.js) Redux Store
- [`App.js`](/App.js) App entrypoint
- [`package.json`](/package.json) React Native Dependencies 


## Dependencies

- [`React Navigation`](https://reactnavigation.org/docs/getting-started) To navigate between screens 
- [`Redux`](https://es.redux.js.org/) To manage app's state with Redux
- [`Redux persist`](https://github.com/rt2zz/redux-persist) To persist Redux store (only language settings)
- [`react-native-async-storage`](https://react-native-async-storage.github.io/async-storage/docs/install/) Engine to store Redux state with redux-persist
- [`react-native-vector-icons`](https://www.npmjs.com/package/react-native-vector-icons) To use Material's community icons
- [`react-native-splash-screen`](https://github.com/crazycodeboy/react-native-splash-screen#readme) To hide splash screen
- [`react-native-wifi-reborn`](https://github.com/JuanSeBestia/react-native-wifi-reborn#readme) To scan wifi networks
- [`react-native-android-open-settings-async`](https://github.com/chris-shaw-2011/react-native-android-open-settings-async/#readme) To open Android's wifi settings
- [`react-native-circular-progress`](https://www.npmjs.com/package/react-native-circular-progress) To show lamp's brightness 
- [`react-native-smooth-slider`](https://github.com/flyskywhy/react-native-smooth-slider#readme) To change lamp's brightness


