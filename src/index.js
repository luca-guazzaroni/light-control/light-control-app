/** Imports: React */
import React from 'react';
import { View } from 'react-native';
/** Imports: Root navigator */
import RootNavigator from './navigation/RootNavigator';


const App = () => {  
  return(
    <RootNavigator/>
  )
} 

export default App;