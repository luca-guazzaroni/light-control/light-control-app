/** Imports: React */
import React from 'react';
import { StyleSheet, View, Text, FlatList } from 'react-native';
/** Imports: Redux */
import { connect } from 'react-redux'; 
import { selectLanguage } from '../../reducers/language';
/** Imports: custom components */
import { ListItem } from '../../components';
import { AppStyles, PALETTE } from '../../constants';


/**
 * SettingsScreen
 * 
 * @param {Object} language language object from Redux.
 * @param {Function} selectLanguage action to select language in Redux store.
 */
const SettingsScreen = ({ 
    language,
    selectLanguage,
    }) => {

    // render
    return (
        <View style={[styles.container, AppStyles.borderShadow]}>
            <Text style={styles.sectionTitle}> 
                {language[language.selected].settings.languages}
            </Text>
            <FlatList
                style={styles.list}
                data={language.options}
                keyExtractor={ x => String(x.code) }
                renderItem={ ({item}) => {
                    return <ListItem
                                style={styles.item}
                                title={item.name}
                                selected={item.code == language.selected}
                                selectedColor={PALETTE.primary}
                                onPress={() => selectLanguage(item.code)}
                            />
                }}
            />
        </View>
    );
};

/** Style */
const styles = StyleSheet.create({
    container: {
        flex: 1, // vertical
        justifyContent: 'flex-start',
        margin: 20,
        padding: 25,
        backgroundColor: PALETTE.background,
        borderRadius: 15,
    },
    sectionTitle: {
        fontSize: 20, 
        color: PALETTE.title,
        fontWeight: 'bold',
        borderBottomWidth: 1, 
        borderBottomColor: PALETTE.divider,
    },
    list: {
        alignContent: 'stretch',
        marginTop: 10,
    },
    item: {		
        // borders
        borderRadius: 10,
        borderWidth : 1,
        borderColor: PALETTE.divider,
    },
});

/** Redux: State to props */
const mapStateToProps = state => {
  return { 
    language: state.language,
  }
}

/** Redux: Actions creators to props */
const mapDispatchToProps = dispatch => ({
    selectLanguage: (data) => dispatch( selectLanguage(data) ),
})

/** Export: screen component connected to the state */
export default connect(mapStateToProps, mapDispatchToProps)(SettingsScreen)