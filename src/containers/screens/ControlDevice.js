/** Imports: React */
import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Switch } from 'react-native';
/** Imports: Redux */
import { connect } from 'react-redux'; 
import { setLightStatus } from '../../reducers/light';
/** Imports: Dependencie Circular progress and Slider */
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Slider from "react-native-smooth-slider";
/** Imports: Icons */
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
/** Imports: api functions */
import { iotDevice } from '../../utils';
/** Imports: Custom button */
import { Button } from '../../components';
/** Imports: constants */
import { AppStyles, PALETTE, ICONS } from '../../constants';


/**
 * ControlDeviceScreen
 * 
 * @param {Object} navigation (react navigation) used to go back to other screen
 * @param {Object} lang contains text depending wich language is selected in Redux state
 * @param {Boolean} lightIsConnected is mobile is connected to a lamp (Redux)
 * @param {Object} lightStatus light status information stored in Redux state
 * @param {Function} setLightStatus updates light's status in Redux state
 */
const ControlDeviceScreen = ({
  navigation, 
  lang,
  lightIsConnected,
  lightStatus,
  setLightStatus,
  }) => {
  // used to handle Switch's state 
  const [switchEnabled, setSwitchEnabled] = useState(false);
  // used to hanlde Slider's state
  const [sliderValue, setSliderValue] = useState(0);

  // Try to change light status. Handle api call and Switch state
  const _handleLightState = async () => {
    let newState = !lightStatus.state;
    setSwitchEnabled(previousState => !previousState);
    // Sending request
    let response = await iotDevice.changeLightState(newState)
    // Check if status is 2xx, 4xx or 5xx
    if( response.ok ) {
      setLightStatus({ state: newState, level: lightStatus.level })
    } else {
      // If fails, switch returns to its previous state
      setSwitchEnabled(previousState => !previousState); 
    }
  }

  // Try to change light level. Handle api call and Slider state
  const _handleLightLevel = async (newValue) => {
    setSliderValue(newValue);
    // Sending request
    let response = await iotDevice.changeLightLevel(newValue);
    // Check if status is 2xx, 4xx or 5xx
    if( response.ok ) {
      setLightStatus({ level: newValue, state: lightStatus.state });
    } else {
      //setSwitchEnabled(previousState => !previousState);
    }
  }

  // Gets light current status when rendering
  const _getLightStatus = async () => {
    const response = await iotDevice.getLightStatus();
    setLightStatus({ state: response.state, level: response.level })
    setSwitchEnabled(response.state);
    setSliderValue(response.level);
  }

  // On rendering
  useEffect(() => {
    if(lightIsConnected){
      _getLightStatus()
    }
  }, [lightIsConnected]);

  // render
  if( !lightIsConnected ){
    return(
      <View style={[styles.container, AppStyles.borderShadow]}>
        <Button 
          title={lang.controlDevice.button} 
          onPress={() => navigation.goBack()} 
          color={PALETTE.buttonText}
          size={20}
          backgroundColor={PALETTE.primary}
          iconName={ICONS.lamp}
          loading={false}
          horizontal={true}
        /> 
      </View>
    )
  }

  // render
  return (
    <View style={[styles.container, AppStyles.borderShadow]}>
      <View style={styles.lightState}>
        <Icon 
          name={lightStatus.state ? ICONS.lampOn : ICONS.lampOff} 
          size={30} 
          color={lightStatus.state ? PALETTE.active : PALETTE.disabled}
        />
        <Text style={{fontSize: 20}}> 
          {lang.controlDevice.title} 
        </Text>
        <Switch
          trackColor={{ false: PALETTE.disabled, true: PALETTE.active }}
          thumbColor={PALETTE.switchThumb}
          onValueChange={() => _handleLightState()}
          value={switchEnabled}
        />
      </View>
      <View style={styles.lightLevel}>
        <AnimatedCircularProgress
          style={{alignSelf:'center', marginTop: 50}}
          size={250}
          width={30}
          duration={0}
          fill={lightStatus.state ? lightStatus.level: 0}
          backgroundColor={PALETTE.progressBackground}
          tintColor={PALETTE.primary}
        >
        { (fill) => (
            <Text 
              style={{
                fontSize: 30, 
                fontWeight: 'bold', 
                color: lightStatus.state ? PALETTE.active : PALETTE.disabled
            }}>
              {lightStatus.level}%
            </Text>
        )}
        </AnimatedCircularProgress>
        <Slider
          minimumValue={0}
          maximumValue={100}
          step={1}
          value={sliderValue}
          useNativeDriver={true}
          moveVelocityThreshold={1000}
          onValueChange={value => _handleLightLevel(value)}
          onSlidingComplete={value => _handleLightLevel(value)}
          thumbTintColor={lightStatus.state ? PALETTE.active : PALETTE.disabled}
          minimumTrackTintColor={PALETTE.transparentPrimary}
        />
      </View>
      
    </View>
  );
}

/** Styles:  */
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    margin: 20,
    flex: 1, // vertical
    padding: 25,
    backgroundColor: PALETTE.background,
    borderRadius: 15,
  },
  lightState: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    borderBottomColor: PALETTE.divider,
    borderBottomWidth: 1,
  },
  lightLevel: {
    flex: 5, 
    justifyContent: 'center', 
    marginLeft: 10,
    marginRight: 10,
    alignItems: "stretch",
    justifyContent: "space-around",
  }
})

/** Redux: State to props */
const mapStateToProps = state => {
  return { 
    lang: state.language[ state.language.selected ],
    lightStatus: state.light.status,
    lightIsConnected: state.connection.isDevice,
  }
}

/** Redux: Actions creators to props */
const mapDispatchToProps = dispatch => ({
  setLightStatus: (data) => dispatch( setLightStatus(data) ),
})

/** Export: screen component connected to the state */
export default connect(mapStateToProps, mapDispatchToProps)(ControlDeviceScreen)