/** Imports: React */
import React, {useState, useEffect} from 'react';
import { StyleSheet, View, Text, FlatList } from 'react-native';
/** Imports: Redux */
import { connect } from 'react-redux'; 
import { setNetworkStatus, setIsDevice } from '../../reducers/connection';
/** Imports: custom components */
import { WifiListItem, Button, Card } from '../../components';
/** Imports: request permission hook*/
import { requestPermission } from '../../utils';
/** Imports: wifi package*/
import WifiManager from 'react-native-wifi-reborn';
/** Imports: open android settings */
import AndroidOpenSettings from 'react-native-android-open-settings-async'
/** Imports: constants */
import { AppStyles, PALETTE, ICONS } from '../../constants';
/** Imports: check if network is from a lamp*/
import { iotDevice } from '../../utils';


/**
 * ScanDevicesScreen
 * 
 * @param {Object} lang contains text depending wich language is selected in Redux state
 * @param {Object} wifiNetwork wifi network information stored in Redux state
 * @param {Function} setNetworkStatus updates wifi's information in Redux state
 * @param {Boolean} wifiIsDevice if current wifi is from a lamp. (Redux state)
 * @param {Function} setWifiIsDevice updates redux state. (isDevice)
 */
const ScanDevicesScreen = ({
  lang,
  wifiNetwork,
  setNetworkStatus,
  wifiIsDevice,
  setWifiIsDevice,
  }) => {
  // request location permission to use wifi scanning
  const { hasPermission, permissionError } = requestPermission();
  // errors from many sources (no used yet)
  const [error, setError] = useState(null);
  // when we are scanning for wifi networks
  const [scanning, setScanning] = useState(false);
  // array with near wifi data
  const [scannedNetworks, setScannedNetworks] = useState([]);

  // when gets permission look for current wifi's information
  useEffect(() => {
    if ( hasPermission && permissionError==null) {
      _setWifiStatus();
    } else {
      setError(permissionError);
    }
  }, [hasPermission]);

  // Get wifi data from phone
  const _getWifiDataFromPhone = async() => {
    const enabled = await WifiManager.isEnabled();
    const connected = await WifiManager.connectionStatus(); 
    const ssid = await WifiManager.getCurrentWifiSSID();
    const bssid = await WifiManager.getBSSID();
    return { enabled, connected, ssid, bssid }
  } 

  // calls _getWifiDataFromPhone and stored result in redux state
  const _setWifiStatus = async () => {
    try {
      const { enabled, connected, ssid, bssid } = await _getWifiDataFromPhone()
      // upload data to redux state
      setNetworkStatus({
        enabled: enabled,
        connected: connected,
        ssid: ssid,
        bssid: bssid,
      });
      setWifiIsDevice( iotDevice.checkIsWifiIsDevice(ssid, bssid) )
    } catch (error) {
      setError(error.message);
    }
  }

  // Open android Wifi settings
  const _openWifiSettings = async () => {
    const a = await AndroidOpenSettings.wifiSettings();
    // when return to app check wifi's information again.
    _setWifiStatus();
  };

  // Scan wifi networks
  const _scanWifinetworks = async () => {
    try {
      setScanning(true);
      const data = await WifiManager.reScanAndLoadWifiList();
      // check which network corresponds to a lamp
      for(let i=0; i<data.length; i++){
        let isDevice = iotDevice.checkIsWifiIsDevice(data[i].SSID, data[i].BSSID)
        data[i].isDevice = isDevice;
      }
      setScanning(false);
      setScannedNetworks(data);
    } catch (error) {
      setError(error);
    }
  }

  // render
  return (
      <View style={styles.container}>
          <View style={styles.cardContainer}>
            <Card
              style={AppStyles.borderShadow}
              title={ 
                wifiNetwork.connected ? 
                lang.scanDevices.connected : 
                lang.scanDevices.disconnected
              }
              description={wifiNetwork.ssid}
              iconName={
                wifiIsDevice === true ?
                ICONS.lamp :
                (wifiNetwork.connected ? ICONS.wifi : ICONS.wifiDisconnected)
              }
              reload={_setWifiStatus}
              iconColor={PALETTE.primary}
              fontSize={20}
            />
          </View>
          <View style={[styles.listContainer, AppStyles.borderShadow]}>
            <Text style={styles.listTitle}>
            {
              scanning ? 
              lang.scanDevices.scanning :
              ( 
                scannedNetworks.length != 0 ?
                lang.scanDevices.founded :
                lang.scanDevices.advice
              )
            }
            </Text>
            { (scannedNetworks.length != 0 && scanning == false) &&
              <FlatList
                  style={styles.list}
                  data={scannedNetworks}
                  keyExtractor={ x => String(x.BSSID) }
                  renderItem={ ({item}) => {
                      return <WifiListItem 
                                style={{borderColor:PALETTE.primary}}
                                title={item.SSID} 
                                description={item.BSSID}
                                signalLevel={item.level}
                                onPress={() => _openWifiSettings()} 
                                highlight={item.isDevice}
                                highlightIcon={{ 
                                  name: ICONS.lamp,
                                  color: PALETTE.primary
                                }}
                              />  
                  }}
              />
            }
          </View>
          <View style={styles.buttonContainer}>
            <Button 
              title={lang.scanDevices.button} 
              onPress={_scanWifinetworks} 
              color={PALETTE.buttonText}
              size={20}
              backgroundColor={PALETTE.primary}
              iconName={ICONS.scanButton}
              loading={scanning}
              horizontal={true}
            /> 
          </View>    
      </View>
  );
};

/** Style */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  cardContainer: {
    flex: 2,
    paddingBottom: 20,
  },
  listContainer: { 
    flex: 5, // vertical
    padding: 25,
    backgroundColor: PALETTE.background,
    borderRadius: 15,
  },
  buttonContainer: {
    flex: 1, 
    justifyContent: 'flex-end',
    marginTop: 10,
  },
  listTitle: {
    color: PALETTE.title,
    fontWeight: 'bold', 
    fontSize: 20,
    padding: 3,
    marginBottom: 10,
    // divider
    borderBottomColor: PALETTE.divider,
    borderBottomWidth: 1,
  }
});

/** Redux: State to props */
const mapStateToProps = state => {
  return { 
    lang: state.language[ state.language.selected ],
    wifiNetwork: state.connection.wifiNetwork,
    wifiIsDevice: state.connection.isDevice,
  }
}

/** Redux: Actions creators to props */
const mapDispatchToProps = dispatch => ({
  setNetworkStatus: (data) => dispatch( setNetworkStatus(data) ),
  setWifiIsDevice: (data) => dispatch( setIsDevice(data) ),
})

/** Export: screen component connected to the state */
export default connect(mapStateToProps, mapDispatchToProps)(ScanDevicesScreen)