/** Imports: React */
import React , {useState, useEffect} from 'react';
import { 
	TouchableOpacity, 
	View,
    Text, 
    StyleSheet,
} from 'react-native';
/** Imports: Icons*/
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


/**
 * WifiListItem
 * 
 * @param {String} title text in bold and black letters
 * @param {String} description text in bold and grey letters
 * @param {Number} signalLevel value used to select wifi's strength icon
 * @param {Boolean} highlight if true shows an icon on the right side
 * @param {Object} highlightIcon icon to show when highlight=true {name: <String>, color: <String>}
 * @param {Object} style Style to add to styles.item
 * @param {Function} onPress Function executed when item is pressed
 */
const WifiListItem = ({
	title, 
	description, 
	signalLevel,
	highlightIcon,
	highlight,
	style,
	onPress 
	}) => {
	// Signal leve icons depends on signalLevel prop
	const [signalIcon, setSignalIcon] = useState(DEFAULT.wifiIconName);

	// Every time signalLevel prop changes
	useEffect(() => {
		if( signalLevel > -68 ){
			setSignalIcon('wifi-strength-4');
		} else if( signalLevel > -75 ) {
			setSignalIcon('wifi-strength-3');
		} else if( signalLevel > -80 ) {
			setSignalIcon('wifi-strength-2');
		} else if( signalLevel > -85) {
			setSignalIcon('wifi-strength-1');
		} else {
			setSignalIcon('wifi-strength-outline');
		}
	}, [signalLevel]);

	// render
	return(
		<TouchableOpacity 
			style={[styles.item, style]} 
			onPress={onPress}
		>
			<View style={{flex: 1}}>
				<Icon 
					name={signalIcon} 
					size={DEFAULT.wifiIconSize} 
					color={DEFAULT.wifiIconColor}
				/>
			</View>
			<View style={{flex: 3}}>
				<Text style={styles.title}> 
					{title} 
				</Text>
				<Text style={styles.description}> 
					{description} 
				</Text>
			</View>
			{
				highlight &&
				<View style={{flex: 1}}>
					<Icon 
						name={highlightIcon.name} 
						size={DEFAULT.highlightIconSize} 
						color={highlightIcon.color}
					/>
				</View>
			}	
		</TouchableOpacity>
	);
}

/** Default values */
const DEFAULT = {
	wifiIconName: 'wifi-strength-outline',
	wifiIconSize: 40,
	wifiIconColor: 'black',
	highlightIconSize: 40,
	color: {
		title: 'black',
		description: 'grey',
		border: 'grey',
		background: 'white',
	}
}

/** Style */
const styles = StyleSheet.create({
	item: {
		flex: 1,
		flexDirection: 'row',
		padding: 15,
		fontSize: 20,
		// margins
		marginBottom: 10,
		marginRight: 15, 
		// borders
		borderRadius: 10,
		borderWidth : 1,
		borderColor: DEFAULT.color.border,
		backgroundColor: DEFAULT.color.background,
	},
	title: {
		fontWeight: 'bold',
		color: DEFAULT.color.title,
	},
	description: {
		fontWeight: 'bold', 
		color: DEFAULT.color.description,
	}
});

/** Export component */
export default WifiListItem;