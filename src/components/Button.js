/** Imports: React */
import React, { useState, useEffect } from 'react';
import { 
    TouchableOpacity, 
    ActivityIndicator,
    StyleSheet,
    Text, 
} from 'react-native';
/** Imports: Icons */
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

/**
 * Button
 * 
 * @param {String} title Button's Text
 * @param {String} backgroundColor Button's background color
 * @param {String} color Icon and Text color
 * @param {Number} size Icon and Text size, height depends of this
 * @param {Boolean} horizontal If false, Text isunder Icon. If True, Icon is in the right of Text
 * @param {String} iconName Icon's name, according to available options in MaterialCommunityIcons library
 * @param {Boolean} loading If true, the button shows an activity indicator instead of Text-Icon
 * @param {Function} onPress Function to execute when the button is pressed
 */
const Button = ({
    title,
    backgroundColor,
    color, 
    size,
    horizontal,
    iconName, 
    loading,
    onPress, 
    }) => {
    // button height fixed to avoid size changes when switching
    // content between icon/text  and activityIndicator
    const [height, setHeight] = useState(DEFAULT.height);

    // Every time size prop changes, height is changed.
    useEffect( () => {
        if(size > DEFAULT.height - DEFAULT.padding){
            setHeight(size + DEFAULT.padding) 
        } else {
            setHeight(DEFAULT.height)
        }      
    }, [size]);

    // Render
	return(
        <TouchableOpacity 
            style={[
                styles.container, {
                height: height,
                backgroundColor: backgroundColor,
                flexDirection: horizontal ? 'row' : 'column',
            }]} 
            onPress={onPress} 
        >
        {
            loading ?
            <ActivityIndicator size={DEFAULT.activityIndicatorSize} color={color} />
            :
            <>
            { !horizontal && <Icon name={iconName} size={size} color={color}/> }
            <Text style={{fontSize: size, color: color, paddingRight: 5}}> 
                {title} 
            </Text> 
            { horizontal && <Icon name={iconName} size={size} color={color}/> }
            </>
        }    
		</TouchableOpacity>
	);
}

/** Default styling values */
const DEFAULT = {
    height: 40,
    padding: 25,
    activityIndicatorSize: 'small',
    containerBorderRadius: 30,
}

/** Style */
const styles = StyleSheet.create({
	container: {
		justifyContent: 'center',
        alignItems: 'center',
        borderRadius: DEFAULT.containerBorderRadius,
        padding: DEFAULT.ActivityIndicator,
	},	
});

/** Export component */
export default Button;