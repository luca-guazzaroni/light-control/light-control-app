/** Imports: React */
import React from 'react';
import { 
	TouchableOpacity, 
	View,
    Text, 
    StyleSheet,
} from 'react-native';
/** Imports: Icons*/
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


/**
 * ListItem
 * 
 * @param {String} title text in bold and black letters
 * @param {Boolean} selected if true shows an icon on the right side
 * @param {String} selectedColor icon's color when selected=true
 * @param {Object} style Style to add to styles.item
 * @param {Function} onPress Function executed when item is pressed
 */
const ListItem = ({
	title, 
    selected,
    selectedColor,
	style,
	onPress 
	}) => {
	// render
	return(
		<TouchableOpacity 
			style={[styles.item, style]} 
			onPress={onPress}
		>
            <View style={{flex: 5}}>
                <Text style={styles.title}> 
                    {title} 
                </Text>
            </View>
			{
                selected &&
                <View style={{flex: 1, alignItems:'flex-end'}}>
                    <Icon 
                        name={DEFAULT.selectedIcon} 
                        size={DEFAULT.iconSize} 
                        color={selectedColor}
                    />
                </View>     
			}	
		</TouchableOpacity>
	);
}

/** Default values */
const DEFAULT = {
    iconSize: 20,
    selectedIcon: 'check-circle',
	color: {
		title: 'black',
		border: 'grey',
		background: 'white',
	}
}

/** Style */
const styles = StyleSheet.create({
	item: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
		padding: 15,
        fontSize: 20,
        backgroundColor: DEFAULT.color.background,
		// margins
		marginBottom: 10,
		marginRight: 15, 
	},
	title: {
        color: DEFAULT.color.title,
		fontWeight: 'bold',
	},
});

/** Export component */
export default ListItem;