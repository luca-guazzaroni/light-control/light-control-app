/** Imports: React */
import React, { useState, useEffect } from 'react';
import { 
    View,
    StyleSheet,
    Text, 
} from 'react-native';
/** Imports: */
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


/**
 * Card
 * 
 * @param {String} title Text in bold and black letters
 * @param {String} description Text in grey letters
 * @param {String} iconName Icon's name, according to available options in MaterialCommunityIcons library
 * @param {String} iconColor Icon's color
 * @param {Number} fontSize Size of letters, note that Icon's size is equal to fontSize*ICON_SIZE_FACTOR
 * @param {Object} style Style to add to styles.container
 */
const Card = ({ 
    title,
    description,
    iconName,
    iconColor,
    fontSize,
    style,
    }) => {
    // Render
	return(
        <View style={[styles.container, style]} >
            <View style={styles.iconContainer}>
                <Icon name={iconName} size={fontSize*ICON_SIZE_FACTOR} color={iconColor}/>
            </View>
            <View style={styles.textContainer}>
                <Text style={[styles.title, {fontSize: fontSize}]}> 
                    {title}  
                </Text> 
                <Text style={[styles.description, {fontSize: fontSize}]}> 
                    {description} 
                </Text>
            </View>
        </View>
	);
}

/** Size of icon is fontSize*ICON_SIZE_FACTOR */
const ICON_SIZE_FACTOR = 3

/** Fixed colors */
const COLORS = {
    background: 'white',
    title: 'black',
    description: 'grey'
};

/** Style: */
const styles = StyleSheet.create({
	container: {
        flexDirection: 'row',
        flex: 1, // horizontal
        flex: 1, // vertical
        backgroundColor: COLORS.background,
        borderRadius: 15,
    },
    iconContainer: {
        flex: 2, // horizontal
        justifyContent: 'center',
        alignItems: 'center',
    },
    textContainer: {
        flex: 3, // horizontal
        flexDirection: 'column',
        justifyContent: 'center',
    },
    title: {
        color: COLORS.title, 
        fontWeight: 'bold'
    },
    description: {
        color: COLORS.description
    }
});

/** Export component */
export default Card;