/**
 * 
 */
const ROOT_URL = 'http://192.168.1.1';
const URL_GET_LIGHT_STATUS = ROOT_URL + '/';
const URL_CHANGE_LIGHT_STATE = ROOT_URL + '/state/';
const URL_CHANGE_LIGHT_LEVEL = ROOT_URL + '/level/';

/**
 * 
 */
const ACCEPTED_SSID = [
  "NodeMCU",
  "Light Bulb IOT",
];

/**
 * 
 */
const ACCEPTED_BSSID = [
  "5E:CF:7F", // Espressif Inc.
]

/**
 * getLightStatus
 * 
 * @returns {Object} return light state in this format
 *                   jsonResponse = {
 *                      level: <Number>,
 *                      state: <Boolean>
 *                   }
 */
export const getLightStatus = async () => {
    const response = await fetch(
        URL_GET_LIGHT_STATUS,
        {
          method: 'GET',
          headers: {
            'Content-type': 'Application/json',
          }
        }
    );
    const jsonResponse = await response.json();
    return jsonResponse;
}

/**
 * changeLightState
 * 
 * @param {Boolean} newState true for turn-on, false for turn-off
 * @returns {Object} response from device (response.ok check if status 2xx)
 */
export const changeLightState = async (newState) => {
    const response = await fetch(
        URL_CHANGE_LIGHT_STATE, 
        {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-type': 'Application/json',        
          },
          body: JSON.stringify({state: newState}),
        }
    );
    return response;
}

/**
 * changeLightLevel
 * 
 * @param {Number} newlevel new light brightness level (between 0 and 100)
 * @returns {Object} response from device (response.ok check if status 2xx)
 */
export const changeLightLevel = async (newlevel) => {
    const response = await fetch(
        URL_CHANGE_LIGHT_LEVEL, 
        {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-type': 'Application/json',        
          },
          body: JSON.stringify({level: newlevel}),
        }
    );
    return response;
}

/**
 * checkIsWifiIsDevice
 * 
 * @param {String} ssid 
 * @param {String} bssid 
 * @returns {Boolean} true if SSID and BSSID provided correspond to 
 *                    the ones defined in ACCEPTED_SSID and ACCEPTED_BSSID
 */
export const checkIsWifiIsDevice = (ssid, bssid) => {
  // check if ssid is in ACCEPTED_SSID array
  let ssidAccepted = ACCEPTED_SSID.includes(ssid)
  // check if bssid is in ACCEPTED_BSSID array
  //let bssidAccepted = ACCEPTED_BSSID.includes(bssid.toUpperCase)
  // if both true, the network is an accepted device
  return ssidAccepted && true//bssidAccepted 
}