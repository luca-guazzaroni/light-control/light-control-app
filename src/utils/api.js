/**
 * 
 */
const url = 'http://192.168.1.1';

/**
 * getLightStatus
 * 
 * @returns {Object} return light state in this format
 *                   jsonResponse = {
 *                      level: <Number>,
 *                      state: <Boolean>
 *                   }
 */
export const getLightStatus = async () => {
    const response = await fetch(
        'http://192.168.1.1/',
        {
          method: 'GET',
          headers: {
            'Content-type': 'Application/json',
          }
        }
    );
    const jsonResponse = await response.json();
    return jsonResponse;
}

/**
 * changeLightState
 * 
 * @param {Boolean} newState true for turn-on, false for turn-off
 * @returns response from server (response.ok check if status 2xx)
 */
export const changeLightState = async (newState) => {
    const response = await fetch(
        'http://192.168.1.1/state/', 
        {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-type': 'Application/json',        
          },
          body: JSON.stringify({state: newState}),
        }
    );
    return response;
}

/**
 * changeLightLevel
 * 
 * @param {Number} newlevel new light brightness level (between 0 and 100)
 * @returns response from server (response.ok check if status 2xx)
 */
export const changeLightLevel = async (newlevel) => {
    const response = await fetch(
        'http://192.168.1.1/level/', 
        {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-type': 'Application/json',        
          },
          body: JSON.stringify({level: newlevel}),
        }
    );
    return response;
}