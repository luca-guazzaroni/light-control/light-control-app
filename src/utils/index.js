import * as iotDevice from './iotDevice';

export { default as requestPermission } from './requestPermission';
export { iotDevice as iotDevice };