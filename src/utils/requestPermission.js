/** Import: React hooks */
import { useState, useEffect } from 'react';
/** Import: Permisisons handler */
import { PermissionsAndroid } from 'react-native';

/**
 * requestPermission
 */
const requestPermission = () => {
    const [hasPermission, setHasPermission] = useState(false);
    const [permissionError, setPermissionError] = useState(null);
  
    const request = async () => {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            setHasPermission(true);
        } else {
            setPermissionError("Location permission denied");
        }
    }
  
    useEffect( () => { 
        request();
    }, [] );
  
    return { hasPermission, permissionError };
}

/** Export */
export default requestPermission;