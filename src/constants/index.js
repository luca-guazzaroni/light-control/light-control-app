export { default as AppStyles } from './styles';
export { default as PALETTE } from './colors';
export { default as ICONS} from './icons';