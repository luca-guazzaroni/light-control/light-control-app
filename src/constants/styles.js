/** Imports: */
import { StyleSheet } from 'react-native';

/** */
const AppStyles = StyleSheet.create({
    borderShadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
});

export default AppStyles;