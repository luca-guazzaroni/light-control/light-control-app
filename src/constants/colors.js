/**
 * Colors to use in app
 */
const PALETTE = {
    primary: 'tomato',
    transparentPrimary: 'rgba(255,99,71,0.3)',
    title: 'black',
    // active disabled components
    active: 'tomato',
    disabled: 'grey',
    // button
    buttonText: 'white',
    // views
    background: 'white',
    // lines 
    divider: 'grey',
    // switch
    switchThumb: '#f4f3f4',
    // circular progress
    progressBackground: "#eeeeee",
    // header
    headerTintColor: 'white',
}

export default PALETTE;