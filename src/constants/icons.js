/**
 * Icons to use in app (Material community)
 */
const ICONS = {
    lamp: 'lightbulb-outline',
    lampOff: 'lightbulb-off-outline',
    lampOn: 'lightbulb-on-outline',
    wifi: 'wifi',
    wifiDisconnected: 'wifi-strength-off-outline',
    scanDevicesTab: 'cast-connected',
    controlDeviceTab: 'lightbulb',
    scanButton: 'magnify',
    settingsButton: 'cog',
}

export default ICONS;