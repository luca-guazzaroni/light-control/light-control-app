/** Imports: Language json files */
import * as en from '../translations/en.json';
import * as es from '../translations/es.json';

/** Initial state */
const initialState =  {
    selected: 'en',
    options: [
        { code: 'en', name: "English" },
        { code: 'es', name: 'Spanish' },
    ],
    en: en,
    es: es,
};
  
/** Actions types */
const SELECT_LANGUAGE = 'SELECT_LANGUAGE';
 
/** Actions creators */
export const selectLanguage = data => ({
    type: SELECT_LANGUAGE,
    payload: data,
})

/** Reducer  */ 
export default (state = initialState, action)  => {
    switch(action.type) {
        case SELECT_LANGUAGE:
            return {
                ...state,
                selected: action.payload
            }
        default:
            return state
    }
}

  