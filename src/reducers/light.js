/** Initial state */
const initialState =  {
    status: {
        state: false,
        level: 0,
    },
};
  
/** Actions types */
const SET_LIGHT_STATUS = 'SET_LIGHT_STATUS';
 
/** Actions creators */
export const setLightStatus = data => ({
    type: SET_LIGHT_STATUS,
    payload: data,
})

/** Reducer  */ 
export default (state = initialState, action)  => {
    switch(action.type) {
        case SET_LIGHT_STATUS:
            return {
                status: action.payload
            }
        default:
            return state
    }
}

  