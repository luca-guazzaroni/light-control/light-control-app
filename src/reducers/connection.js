/** Initial state */
const initialState =  {
    isDevice: false,
    wifiNetwork: {
        enabled: false,
        connected: false,
        bssid: '',
        ssid: '',
    },
};
  
/** Actions types */
const SET_NETWORK_STATUS = 'SET_NETWORK_STATUS';
const SET_IS_DEVICE = 'SET_IS_DEVICE';
 
/** Actions creators */
export const setNetworkStatus = data => ({
    type: SET_NETWORK_STATUS,
    payload: data,
})

export const setIsDevice = data => ({
    type: SET_IS_DEVICE,
    payload: data,
})

/** Reducer  */ 
export default (state = initialState, action)  => {
    switch(action.type) {
        case SET_NETWORK_STATUS:
            return {
                ...state,
                wifiNetwork: action.payload
            }
        case SET_IS_DEVICE:
            return {
                ...state,
                isDevice: action.payload
            }
        default:
            return state
    }
}

  