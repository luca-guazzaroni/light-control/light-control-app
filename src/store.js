/** Imports: dependencies Redux */
import { createStore, combineReducers } from 'redux';
/** Imports: dependencies Redux persist */
import { persistStore, persistReducer } from 'redux-persist'
/** Imports: App's state reducers */
import * as reducers from './reducers'
/** Imports: Storage persistent Redux's state */
import AsyncStorage from '@react-native-async-storage/async-storage';

/** Combine all reducers */
const rootReducer = combineReducers({
  ...reducers,
})

/** Redux persist configuration (only language settings are persistant) */
const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['language'],
}

/** Persist Reducer */
const persistedReducer = persistReducer(persistConfig, rootReducer)

/** Store */
export const store = createStore(persistedReducer)

/** Persisted Store (only language by now) */
export const persistedStore = persistStore(store)



