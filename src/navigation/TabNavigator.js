/** Imports: React */
import React from 'react';
/** Imports: React Navigation */
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
/** Imports: Icons */
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
/** Imports: Screen components */
import { ScanDevicesScreen, ControlDeviceScreen } from '../containers/screens';
/** imports: Colors and icon's names */
import { PALETTE, ICONS } from '../constants';
/** Imports: Redux */
import { connect } from 'react-redux'; 


/** Create bottom tab navigator */
const Tab = createBottomTabNavigator();

/**
 * Icons for each screen
 */
const screenOptions = ({ route }) => ({
  tabBarIcon: ({ focused, color, size }) => {
    let iconName;
    if (route.name === 'ScanDevices') {
      iconName = ICONS.scanDevicesTab;
    } else if (route.name === 'ControlDevice') {
      iconName = ICONS.controlDeviceTab;
    }
    return <Icon name={iconName} size={size} color={color} />;
  },
})

/**
 * Tabs style
 */
const tabBarOptions = {
  activeTintColor: PALETTE.active,
  inactiveTintColor: PALETTE.disabled,
  style:{
    height:100,
  },
  tabStyle: {
    padding: 20,
  },
  labelStyle: {
    fontSize: 15,
  },
}

/**
 * Tab navigator
 * 
 * @param {Object} lang contains text depending wich language is selected in Redux state
 */
const TabNavigator = ({ lang }) => {
  // render
  return(
    <Tab.Navigator
      screenOptions={screenOptions}
      tabBarOptions={tabBarOptions}   
    >
      <Tab.Screen 
        name="ScanDevices" 
        component={ScanDevicesScreen}
        options={{tabBarLabel: lang.tabBar.scanDevices}}
      />
      <Tab.Screen 
        name="ControlDevice" 
        component={ControlDeviceScreen}
        options={{tabBarLabel: lang.tabBar.controlDevice}}
      />
    </Tab.Navigator>
  )
}

/** Redux: State to props */
const mapStateToProps = state => {
  return { 
    lang: state.language[ state.language.selected ],
  }
}

/** Export: tab navigator connected to the state */
export default connect(mapStateToProps)(TabNavigator)
