/** Imports: React */
import React from 'react';
/** Imports: React navigation */
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
/** Imports: Nested navigator */
import TabNavigator from './TabNavigator';
/** Imports: Screen */
import { SettingsScreen } from '../containers/screens';
/** Imports: colors */
import { PALETTE, ICONS } from '../constants';
/** Imports: Redux */
import { connect } from 'react-redux'; 
/** Imports: Custom buttons */
import { Button } from '../components';

/** Create stack navigator */
const Stack = createStackNavigator();

/** Header style */
const screenOptions = {
  headerStyle: {
    backgroundColor: PALETTE.primary,
  },
  headerTintColor: PALETTE.headerTintColor,
  headerTitleStyle: {
    fontWeight: 'bold',
  },
}

/**
 * Root navigator
 * 
 * @param {Object} lang contains text depending wich language is selected in Redux state
 */
const RootNavigator = ({ lang }) => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={screenOptions}>
        <Stack.Screen 
          name="TabNavigator" 
          component={TabNavigator} 
          options={
            ({ navigation, route }) => ({
              title: lang.header,
              headerRightContainerStyle:{ 
                marginRight: 15,
              },
              headerRight: () => (
                <Button 
                  title="" 
                  onPress={() => navigation.navigate('Settings')}
                  color={PALETTE.buttonText}
                  size={25}
                  backgroundColor={PALETTE.primary}
                  iconName={ICONS.settingsButton}
                  loading={false}
                  horizontal={true}
                />
              ),
            })
          }
        />
        <Stack.Screen 
          name="Settings" 
          component={SettingsScreen} 
          options={{
            title: lang.settings.header
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

/** Redux: State to props */
const mapStateToProps = state => {
  return { 
    lang: state.language[ state.language.selected ],
  }
}

/** Export: Root navigator connected to the state */
export default connect(mapStateToProps)(RootNavigator)